var pageSession = new ReactiveDict();

Template.TracksDetails.rendered = function() {
	// Options for Views and Performance Chart
    var lineData = {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        datasets: [
            {
                label: "Line 2",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: [22, 44, 67, 43, 76, 45, 62, 35, 40, 50, 50, 25]
            },
            {
                label: "Line 1",
                fillColor: "rgba(0,102,203,0.5)",
                strokeColor: "rgba(0,102,203,0.7)",
                pointColor: "rgba(0,102,203,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(26,179,148,1)",
                data: [16, 32, 18, 26, 42, 33, 44, 30, 80, 70, 30, 35]
            }
        ]
    };

    var lineOptions = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 1,
        datasetFill : true,
        responsive: true
    };

    var ctx = document.getElementById("lineOptions-by-chat-platform").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData, lineOptions);	



   // Options for gender and Age
    var barOptions = {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        barShowStroke : true,
        barStrokeWidth : 1,
        barValueSpacing : 10,
        barDatasetSpacing : 1,
        responsive:true
    };

    var barData = {
        labels: ["12 or less", "12 to 18", "19 to 25", "26 to 35", "36 to 45", "46 or more"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,0.5)",
                strokeColor: "rgba(76,148,218,0.8)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [65, 59, 80, 81, 56, 55]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,0.5)",
                strokeColor: "rgba(233,127,117,0.8)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [28, 48, 40, 19, 86, 27]
            }
        ]
    };

    var ctx = document.getElementById("barOptions-gender-age").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData, barOptions); 






};

Template.TracksDetails.events({
	
});

Template.TracksDetails.helpers({
	
});

Template.TracksDetailsDetailsForm.rendered = function() {
	

	pageSession.set("tracksDetailsDetailsFormInfoMessage", "");
	pageSession.set("tracksDetailsDetailsFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();

	//$(".input-group.date").each(function() {
	//	var format = $(this).find("input[type='text']").attr("data-format");
    //
	//	if(format) {
	//		format = format.toLowerCase();
	//	}
	//	else {
	//		format = "mm/dd/yyyy";
	//	}
    //
	//	$(this).datepicker({
	//		autoclose: true,
	//		todayHighlight: true,
	//		todayBtn: true,
	//		forceParse: false,
	//		keyboardNavigation: false,
	//		format: format
	//	});
	//});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.TracksDetailsDetailsForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("tracksDetailsDetailsFormInfoMessage", "");
		pageSession.set("tracksDetailsDetailsFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var tracksDetailsDetailsFormMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(tracksDetailsDetailsFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("tracksDetailsDetailsFormInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("tracksDetailsDetailsFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		Router.go("tracks", {});
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		Router.go("tracks", {});
	}

	
});

Template.TracksDetailsDetailsForm.helpers({
	"infoMessage": function() {
		return pageSession.get("tracksDetailsDetailsFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("tracksDetailsDetailsFormErrorMessage");
	}
	
});
