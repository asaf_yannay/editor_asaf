var pageSession = new ReactiveDict();

Template.Tracks.rendered = function() {
	
};

Template.Tracks.events({

    'click .showhide': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        var icon = $(event.target).closest('i');
        var body = hpanel.find('div.panel-body');
        var footer = hpanel.find('div.panel-footer');
        body.slideToggle(300);
        footer.slideToggle(200);

        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        hpanel.toggleClass('').toggleClass('panel-collapse');
        setTimeout(function () {
            hpanel.resize();
            hpanel.find('[id^=map-]').resize();
        }, 50);
    },

    'click .closebox': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        hpanel.remove();
    }

});

Template.Tracks.helpers({
	
});

var TracksViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("TracksViewSearchString");
	var sortBy = pageSession.get("TracksViewSortBy");
	var sortAscending = pageSession.get("TracksViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["name", "description", "sdate", "edate", "active"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var TracksViewExport = function(cursor, fileType) {
	var data = TracksViewItems(cursor);
	var exportFields = ["name", "description", "sdate", "edate", "active"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.TracksView.rendered = function() {
	pageSession.set("TracksViewStyle", "table");
	
};

Template.TracksView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("TracksViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("TracksViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("TracksViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("tracks.insert", {});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		TracksViewExport(this.tracks, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		TracksViewExport(this.tracks, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		TracksViewExport(this.tracks, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		TracksViewExport(this.tracks, "json");
	}

	
});

Template.TracksView.helpers({

	

	"isEmpty": function() {
		return !this.tracks || this.tracks.count() == 0;
	},
	"isNotEmpty": function() {
		return this.tracks && this.tracks.count() > 0;
	},
	"isNotFound": function() {
		return this.tracks && pageSession.get("TracksViewSearchString") && TracksViewItems(this.tracks).length == 0;
	},
	"searchString": function() {
		return pageSession.get("TracksViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("TracksViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("TracksViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("TracksViewStyle") == "gallery";
	}

	
});


Template.TracksViewTable.rendered = function() {
	
};

Template.TracksViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("TracksViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("TracksViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("TracksViewSortAscending") || false;
			pageSession.set("TracksViewSortAscending", !sortAscending);
		} else {
			pageSession.set("TracksViewSortAscending", true);
		}
	}
});

Template.TracksViewTable.helpers({
	"tableItems": function() {
		return TracksViewItems(this.tracks);
	}
});


Template.TracksViewTableItems.rendered = function() {
	
};

Template.TracksViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();
		
		Router.go("tracks.details", {trackId: this._id});
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		Tracks.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						Tracks.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("tracks.edit", {trackId: this._id});
		return false;
	}
});

Template.TracksViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }
	

	
});
