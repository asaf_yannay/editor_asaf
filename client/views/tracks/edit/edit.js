var pageSession = new ReactiveDict();

Template.TracksEdit.rendered = function() {
	
};

Template.TracksEdit.events({
	
});

Template.TracksEdit.helpers({
	
});

Template.TracksEditEditForm.rendered = function() {
	

	pageSession.set("tracksEditEditFormInfoMessage", "");
	pageSession.set("tracksEditEditFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();

	var elemActive = document.querySelector('.js-switchActive');
	var initActive = new Switchery(elemActive);

	var elemFB = document.querySelector('.js-switchFB');
	var initFB = new Switchery(elemFB);

	var elemKik = document.querySelector('.js-switchKik');
	var initKik = new Switchery(elemKik);

	//$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

	//$(".input-group.date").each(function() {
	//	var format = $(this).find("input[type='text']").attr("data-format");
    //
	//	if(format) {
	//		format = format.toLowerCase();
	//	}
	//	else {
	//		format = "mm/dd/yyyy";
	//	}
    //
	//	$(this).datepicker({
	//		autoclose: true,
	//		todayHighlight: true,
	//		todayBtn: true,
	//		forceParse: false,
	//		keyboardNavigation: false,
	//		format: format
	//	});
	//});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.TracksEditEditForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("tracksEditEditFormInfoMessage", "");
		pageSession.set("tracksEditEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var tracksEditEditFormMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(tracksEditEditFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("tracksEditEditFormInfoMessage", message);
					}; break;
				}
			}

			Router.go("tracks", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("tracksEditEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {


				Tracks.update({ _id: t.data.track._id }, { $set: values }, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("tracks", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.TracksEditEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("tracksEditEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("tracksEditEditFormErrorMessage");
	}
	
});
