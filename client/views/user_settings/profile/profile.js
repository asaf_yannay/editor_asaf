var pageSession = new ReactiveDict();

Template.UserSettingsProfile.onRendered(function(){

    // Animate panel function
    $.fn['animatePanel'] = function() {

        var element = $(this);
        var effect = $(this).data('effect');
        var delay = $(this).data('delay');
        var child = $(this).data('child');

        // Set default values for attrs
        if(!effect) { effect = 'fadeIn'}
        if(!delay) { delay = 0.06 } else { delay = delay / 10 }
        if(!child) { child = '.row > div'} else {child = "." + child}

        //Set defaul values for start animation and delay
        var startAnimation = 0;
        var start = Math.abs(delay) + startAnimation;

        // Get all visible element and set opacity to 0
        var panel = element.find(child);
        panel.addClass('opacity-0');

        // Get all elements and add effect class
        panel = element.find(child);
        panel.addClass('stagger').addClass('animated-panel').addClass(effect);

        var panelsCount = panel.length + 10;
        var animateTime = (panelsCount * delay * 10000000) / 10;

        // Add delay for each child elements
        panel.each(function (i, elm) {
            start += delay;
            var rounded = Math.round(start * 10) / 10;
            $(elm).css('animation-delay', rounded + 's');
            // Remove opacity 0 after finish
            $(elm).removeClass('opacity-0');
        });

        // Clear animation after finish
        setTimeout(function(){
            $('.stagger').css('animation', '');
            $('.stagger').removeClass(effect).removeClass('animated-panel').removeClass('stagger');
        }, animateTime)
    };

    $('.animate-panel').animatePanel();

});


Template.UserSettingsProfile.rendered = function() {
	
};

Template.UserSettingsProfile.events({
	
});

Template.UserSettingsProfile.helpers({
	
});

Template.UserSettingsProfileEditForm.rendered = function() {



	pageSession.set("userSettingsProfileEditFormInfoMessage", "");
	pageSession.set("userSettingsProfileEditFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();


	var elemWha = document.querySelector('.js-switchWha');
	var initWha = new Switchery(elemWha);

	var elemFB = document.querySelector('.js-switchFB');
	var initFB = new Switchery(elemFB);

	var elemWe = document.querySelector('.js-switchWe');
	var initWe = new Switchery(elemWe);

	var elemKik = document.querySelector('.js-switchKik');
	var initKik = new Switchery(elemKik);

	var elemLi = document.querySelector('.js-switchLi');
	var initLi = new Switchery(elemLi);

	var elemVi = document.querySelector('.js-switchVi');
	var initVi = new Switchery(elemVi);		

	var elemTe = document.querySelector('.js-switchTe');
	var initTe = new Switchery(elemTe);

	var elemSl = document.querySelector('.js-switchSl');
	var initSl = new Switchery(elemSl);	

	var elemKa = document.querySelector('.js-switchKa');
	var initKa = new Switchery(elemKa);	

	var elemTa = document.querySelector('.js-switchTa');
	var initTa = new Switchery(elemTa);		

	var elemSn = document.querySelector('.js-switchSn');
	var initSn = new Switchery(elemSn);

	var elemSk = document.querySelector('.js-switchSk');
	var initSk = new Switchery(elemSk);	

	var elemQq = document.querySelector('.js-switchQq');
	var initQq = new Switchery(elemQq);	

	var elemHi = document.querySelector('.js-switchHi');
	var initHi = new Switchery(elemHi);

	var elemNi = document.querySelector('.js-switchNi');
	var initNi = new Switchery(elemNi);	

	var elemIm = document.querySelector('.js-switchIm');
	var initIm = new Switchery(elemIm);	


	
};

Template.UserSettingsProfileEditForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("userSettingsProfileEditFormInfoMessage", "");
		pageSession.set("userSettingsProfileEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var userSettingsProfileEditFormMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(userSettingsProfileEditFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "All changes were saved.";
						pageSession.set("userSettingsProfileEditFormInfoMessage", message);
					}; break;
				}
			}

			Router.go("user_settings.profile", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Something went wrong, please try again.";
			pageSession.set("userSettingsProfileEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				Meteor.call("updateUserAccount", t.data.current_user_data._id, values, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.UserSettingsProfileEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("userSettingsProfileEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("userSettingsProfileEditFormErrorMessage");
	}
	
});
