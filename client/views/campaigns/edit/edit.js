var pageSession = new ReactiveDict();

$(".various").fancybox({
	maxWidth	: 1600,
	maxHeight	: 1200,
	fitToView	: false,
	width		: '100%',
	height		: '100%',
	autoSize	: false,
	closeClick	: false,
	openEffect	: 'none',
	closeEffect	: 'none'
});

Template.CampaignsEdit.rendered = function() {
	
};

Template.CampaignsEdit.events({
	
});

Template.CampaignsEdit.helpers({
	
});

//Template.CampaignsEditEditForm.getKeyword = function() {
//	return Campaigns.find();
//};

Template.CampaignsEditEditForm.rendered = function() {
	

	pageSession.set("campaignsEditEditFormInfoMessage", "");
	pageSession.set("campaignsEditEditFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();

	var elemActive = document.querySelector('.js-switchActive');
	var initActive = new Switchery(elemActive);

	//var elemWha = document.querySelector('.js-switchWha');
	//var initWha = new Switchery(elemWha);

	var elemFB = document.querySelector('.js-switchFB');
	var initFB = new Switchery(elemFB);
    //
	// var elemWe = document.querySelector('.js-switchWe');
	// var initWe = new Switchery(elemWe);

	var elemKik = document.querySelector('.js-switchKik');
	var initKik = new Switchery(elemKik);

	// var elemLi = document.querySelector('.js-switchLi');
	// var initLi = new Switchery(elemLi);

	// var elemVi = document.querySelector('.js-switchVi');
	// var initVi = new Switchery(elemVi);		

	var elemTe = document.querySelector('.js-switchTe');
	var initTe = new Switchery(elemTe);

	var elemKa = document.querySelector('.js-switchKa');
	var initKa = new Switchery(elemKa);	
    //
	// var elemTa = document.querySelector('.js-switchTa');
	// var initTa = new Switchery(elemTa);		

	// var elemSn = document.querySelector('.js-switchSn');
	// var initSn = new Switchery(elemSn);

	var elemSk = document.querySelector('.js-switchSk');
	var initSk = new Switchery(elemSk);	
    //
	// var elemQq = document.querySelector('.js-switchQq');
	// var initQq = new Switchery(elemQq);	
    //
	// var elemHi = document.querySelector('.js-switchHi');
	// var initHi = new Switchery(elemHi);

	var elemNi = document.querySelector('.js-switchSl');
	var initNi = new Switchery(elemNi);


	var elemFacebookAccount = document.getElementsByName('facebook_acount');
	var initFacebookAccount = new Switchery(elemFacebookAccount);
	

	//$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

	//$(".input-group.date").each(function() {
	//	var format = $(this).find("input[type='text']").attr("data-format");
    //
	//	if(format) {
	//		format = format.toLowerCase();
	//	}
	//	else {
	//		format = "mm/dd/yyyy";
	//	}
    //
	//	$(this).datepicker({
	//		autoclose: true,
	//		todayHighlight: true,
	//		todayBtn: true,
	//		forceParse: false,
	//		keyboardNavigation: false,
	//		format: format
	//	});
	//});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.CampaignsEditEditForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("campaignsEditEditFormInfoMessage", "");
		pageSession.set("campaignsEditEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var campaignsEditEditFormMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(campaignsEditEditFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("campaignsEditEditFormInfoMessage", message);
					}; break;
				}
			}

			Router.go("campaigns", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsEditEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {


				Campaigns.update({ _id: t.data.campaign._id }, { $set: values }, function(e) { if(e) errorAction(e); else submitAction(); });

				Campaigns.update({_id: t.data.campaign._id },{$unset:{keyword:1}});
                var keywordKeys = document.getElementsByClassName('keyword-key');
                var keywordValues = document.getElementsByClassName('keyword-value');
                var keywords = {};
                for (var i=0; i < keywordKeys.length; i++)
                    keywords['keyword.' + keywordKeys[i].value] = keywordValues[i].value;
                Campaigns.update({_id: t.data.campaign._id }, { $set: keywords });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("campaigns", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click #insertKey" : function(e,t) {

		event.preventDefault();
		var key = document.getElementById('keyword_key').value;
		var value = document.getElementById('keyword_value').value;
		var wrapperKey = {};
		wrapperKey['keyword.' +key] = value;
		Campaigns.update({_id: t.data.campaign._id}, {$set: wrapperKey});
	},
	"click #removeKey" : function(e,t) {
		event.preventDefault();
		var str = e.currentTarget.className;
		var key = str.substring(28);
		var wrapperKey = {};
		wrapperKey['keyword.' + key] = 'empty';
		Campaigns.update({_id: t.data.campaign._id}, {$set: wrapperKey});
	},
	"click #insertKeyRSS" : function(e,t) {

		event.preventDefault();

		var tkey = document.getElementById('rss_key').value;
		var tvalue = document.getElementById('rss_value').value;

		var wrapperKey = {};
		var key = tkey;
		wrapperKey['rss.' +key] = tvalue;

		Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success save rss "); });


	},
	"click #removeKeyRSS" : function(e,t) {
		event.preventDefault();
		var tkey = document.getElementById('rss_key').value;
		var tvalue = document.getElementById('rss_value').value;

		var wrapperKey = {};
		var key = tkey;
		wrapperKey['rss.' +key] = 'empty';


		Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success remove rss "); });
	},
	"click #form-submit-button-dup" : function(e,t) {
		event.preventDefault();
		var campaignName = document.getElementById('campaignName').value;

		//var wrapperKey = {};
		//var key = tkey;
		//wrapperKey['keyword.' +key] = tvalue;

		/*var wrapperCamp = {};
		var key = "name";
		wrapperCamp[key] = campaignName;

		var wrapperObj = {};
		var key = "campaign";
		wrapperObj[key] = t.data.campaign.campaign;*/

		//var tkey = document.getElementById('keyword_key').value;
		//var tvalue = document.getElementById('keyword_value').value;

		var wrapperKey = {};
		var key = 'campaign';
		wrapperKey['campaign.' +key] = t.data.campaign.campaign;

		var ncid = new Meteor.Collection.ObjectID();



		//var cc = Campaigns.find( { _id: t.data.campaign._id  } );
		//console.log("cc = "+cc.campaign+" obj="+cc.campaign.obj);
		//console.log("cc2 = "+t.data.campaign.campaign+" obj="+t.data.campaign.obj);

		/*Campaigns.insert( {$set: wrapperObj }, {$set: wrapperCamp }, function (e) {
			console.log("Success duplicate ");
		});*/



		//console.log(t.data.campaign._id+" ****************************** ncid="+ncid);
		/*Campaigns.insert({ _id: ncid }, { $set: wrapperCamp },{ upsert: true }, function(e) {console.log("Success duplicate "+campaignName);  });*/

		//Campaigns.insert({  _id: ncid}, { $set: wrapperKey },function(e) {  console.log("Success duplicate "); });
		//Campaigns.insert( {  _id: ncid}, { $set: wrapperCamp },function(e) {  console.log("Success save wrapperCamp "); });


		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				    values.campaign = t.data.campaign.campaign;
				    values.name = campaignName;
					Campaigns.insert(values, function (e) {
						console.log("Success save wrapperCamp ");
						Router.go("campaigns", {});
					});

			}
		);

	}
	//"click #fb-validate-button" : function(e,t) {
	//	event.preventDefault();
	//	var facebook_platform = document.getElementById('facebook_platform').value;
	//	var facebook_webhook = document.getElementById('facebook_webhook').value;
	//	var facebook_botmanager = document.getElementById('facebook_botmanager').value;
	//	var facebook_account = document.getElementById('facebook_account').value;
	//	var facebook_token = document.getElementById('facebook_token').value;
    //
	//	/*the url = "";
    //
	//	Meteor.http.call("GET",url,function(error,result){
	//		console.log("Try to validate fb="+result.statusCode);
	//	});*/
	//	//var wrapperKey = {"facebook_platform":facebook_platform};
	//	//Campaigns.update({  _id: t.data.campaign._id }, { $set: wrapperKey },function(e) {  console.log("Success remove rss "); });
	//},
	//"click #kik-validate-button" : function(e,t) {
	//	event.preventDefault();
	//	var kik_platform = document.getElementById('kik_platform').value;
	//	var kik_webhook = document.getElementById('kik_webhook').value;
	//	var kik_botmanager = document.getElementById('kik_botmanager').value;
	//	var kik_account = document.getElementById('kik_account').value;
	//	var kik_token = document.getElementById('kik_token').value;
    //
	//	var campaignName = document.getElementById('campaignName').value;
    //
	//	var campaignName = document.getElementById('campaignName').value;
    //
	//	url = kik_platform+"campaignName="+campaignName+"&webHook="+kik_webhook+"&user="+kik_account+"&token="+kik_token+"&botManager="+kik_botmanager;
    //
	//	Meteor.http.call("GET",url,function(error,result){
	//		console.log("Try to validate fb="+result.statusCode);
	//	});
	//},
	//"click #telegram-validate-button" : function(e,t) {
	//	event.preventDefault();
	//	var telegram_platform = document.getElementById('telegram_platform').value;
	//	var telegram_webhook = document.getElementById('telegram_webhook').value;
	//	var telegram_botmanager = document.getElementById('telegram_botmanager').value;
	//	var telegram_account = document.getElementById('telegram_account').value;
	//	var telegram_token = document.getElementById('telegram_token').value;
    //
	//	var campaignName = document.getElementById('campaignName').value;
    //
	//	url = telegram_platform+"campaignName="+campaignName+"&webHook="+telegram_webhook+"&user="+telegram_account+"&token="+telegram_token+"&botManager="+telegram_botmanager;
    //
	//	 Meteor.http.call("GET",url,function(error,result){
	//	 console.log("Try to validate fb="+result.statusCode);
	//	 });
    //
	//}


	
});

//platform = http://botmanager.betterbrand.im/KikServer/RequesInit?
//campaignName
//webHook
//user
//token
//botManager
//http://botmanager.betterbrand.im/KikServer/RequesInit?campaignName=w3&webHook=wh&&user=1&token=1&botManager=123


Template.CampaignsEditEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("campaignsEditEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsEditEditFormErrorMessage");
	},
	keyword: function(){
		return _.map(this.keyword, function(value, key){
			return {
				key: key,
				value: value
			};
		});
	},
	rss: function(){
		return _.map(this.rss, function(value, key){
			return {
				key: key,
				value: value
			};
		});
	}
	
});

Template.CampaignsEditEditForm.camp = function() {

	console.log("Campaigns: "+Campaigns.find());

	return Campaigns.find();
};

Template.registerHelper('compare', function(v1, v2) {
	if (typeof v1 === 'object' && typeof v2 === 'object') {
		return _.isEqual(v1, v2); // do a object comparison
	} else {
		return v1 === v2;
	}
});
