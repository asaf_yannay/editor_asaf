Template.AudienceInsert.rendered = function() {

	// Initialize iCheck plugin
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    });

    // Select2 initialize
    $(".js-source-states-1").select2();
    $(".js-source-states-2").select2();
    $(".js-source-states-3").select2();

    var elemWha = document.querySelector('.js-switchWha');
	var initWha = new Switchery(elemWha);

	var elemFB = document.querySelector('.js-switchFB');
	var initFB = new Switchery(elemFB);

	var elemWe = document.querySelector('.js-switchWe');
	var initWe = new Switchery(elemWe);

	var elemKik = document.querySelector('.js-switchKik');
	var initKik = new Switchery(elemKik);

	var elemLi = document.querySelector('.js-switchLi');
	var initLi = new Switchery(elemLi);

	var elemVi = document.querySelector('.js-switchVi');
	var initVi = new Switchery(elemVi);		

	var elemTe = document.querySelector('.js-switchTe');
	var initTe = new Switchery(elemTe);

	var elemKa = document.querySelector('.js-switchKa');
	var initKa = new Switchery(elemKa);	

	var elemTa = document.querySelector('.js-switchTa');
	var initTa = new Switchery(elemTa);		

	var elemSn = document.querySelector('.js-switchSn');
	var initSn = new Switchery(elemSn);

	var elemSk = document.querySelector('.js-switchSk');
	var initSk = new Switchery(elemSk);	

	var elemQq = document.querySelector('.js-switchQq');
	var initQq = new Switchery(elemQq);	

	var elemHi = document.querySelector('.js-switchHi');
	var initHi = new Switchery(elemHi);

	var elemNi = document.querySelector('.js-switchNi');
	var initNi = new Switchery(elemNi);	

	var elemIm = document.querySelector('.js-switchIm');
	var initIm = new Switchery(elemIm);
	
};