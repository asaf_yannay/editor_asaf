/*
Template.dashboard.events({

    click: function(e) {
        var handle = Meteor.subscribe("tracks_single");
        dat = Tracks.find({platform:  "Facebook"}).fetch();
        console.log( dat );
        console.log( dat[0].campaignName );
    }
});*/

//var userParticipation = 10;

var fb = "Facebook";
var kik = "Kik";



Template.dashboard.helpers({


});

//FB precent = fb count / total count * 100

Template.dashboard.userParticipation = function() {
    return Tracks.find().count();
};

Template.dashboard.userParticipationPrecent = function() {
    return Tracks.find().count() * 100;
};

Template.dashboard.userParticipationFBavgprect = function() {
    userParticipationFBavgprect = Tracks.find({platform:  fb}).count() / Tracks.find().count() * 100;
    return userParticipationFBavgprect
};

Template.dashboard.userParticipationKikavgprect = function() {
    userParticipationKikavgprect =  Tracks.find({platform:  kik}).count() / Tracks.find().count() * 100;
    return userParticipationKikavgprect;
};


Template.dashboard.userParticipationPrecent_FB = function() {
    return Tracks.find({platform:  fb}).count() * 100;
};

Template.dashboard.userParticipationPrecent_Kik = function() {
    return Tracks.find({platform:  kik}).count() * 100;
};

function getMostActiveChatPlatform(){
    var facebookCount = Tracks.find({platform:  fb} ).count();
    var kikCount      = Tracks.find({platform:  kik}      ).count();

    if( facebookCount > kikCount ){
        return "Facebook";
    }else{
        return "Kik";
    }
}

Template.dashboard.mostActiveChatPlatform = function() {
    return getMostActiveChatPlatform();
};


Template.dashboard.mostActiveByIcon = function() {
    if ( getMostActiveChatPlatform() == "Facebook" ){
        return "/images/apps/app-logo-messenger.png";
    }else
    if ( getMostActiveChatPlatform() == "Kik" ){
        return "/images/apps/app-logo-kik.png";
    }
}

Template.dashboard.mostActiveByView = function() {
    if ( getMostActiveChatPlatform() == "Facebook" ){
        return Tracks.find({platform:  "Facebook"} ).count();
    }else
    if ( getMostActiveChatPlatform() == "Kik" ){
        return Tracks.find({platform:  "Kik"}      ).count();
    }
}






Template.dashboard.onRendered(function(){

    var handle = Meteor.subscribe("tracks_single");


    Tracker.autorun(function() {
        if (handle.ready()){
            var dat = Tracks.find({platform:  "Facebook"} ,{fields: {platform:1, campaignName:1}}).fetch();
            var facebookCount = Tracks.find({platform:  "Facebook"} ,{fields: {platform:1, campaignName:1}}).count();
            //var dat = Tracks.find({} ,{fields: {platform:1, campaignName:1}}).fetch();
            console.log( dat );
            console.log( dat[0].campaignName );
            console.log("FB count="+facebookCount);

            var labels = dat.map(function(d) { return d.platform });
            var data   = dat.map(function(d) {return d.campaignName });
            console.log("labels="+labels+" data=="+data);

            var userParticipationFBavgprect = Tracks.find({platform:  fb}).count() / Tracks.find().count() * 100;
            var userParticipationKikavgprect =  Tracks.find({platform:  kik}).count() / Tracks.find().count() * 100;

            console.log("FB = "+userParticipationFBavgprect);
            console.log("Kik = "+userParticipationKikavgprect);

            //userParticipation = Tracks.find().count();


            $('#datapicker-analytics-1').datepicker();
            $('#datapicker-analytics-2').datepicker();

            // Options for Campaigns View top box chart

            var chartIncomeData = [
                {
                    label: "line",
                    data: [ [1, 1], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
                }
            ];

            var chartIncomeOptions = {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: true,
                        fillColor: "#0066cb"

                    }
                },
                colors: ["0066cb"],
                grid: {
                    show: false
                },
                legend: {
                    show: false
                }
            };

            $.plot($("#campaigns-views-chart"), chartIncomeData, chartIncomeOptions);







            // Options for Views and Performance Chart
            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Line 2",
                        fillColor: "rgba(159,159,159,0.5)",
                        strokeColor: "rgba(159,159,159,1)",
                        pointColor: "rgba(159,159,159,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(159,159,159,1)",
                        data: [22, 44, 67, 43, 76, 45, 12]
                    },
                    {
                        label: "Line 1",
                        fillColor: "rgba(0,102,203,0.5)",
                        strokeColor: "rgba(0,102,203,0.7)",
                        pointColor: "rgba(0,102,203,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: [16, 32, 18, 26, 42, 33, 44]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 20,
                datasetStroke : true,
                datasetStrokeWidth : 1,
                datasetFill : true,
                responsive: true
            };

            var ctx = document.getElementById("lineOptions").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);




            // Options for Response Rate
            var doughnutData = [
                {
                    value: userParticipationFBavgprect,
                    color:"#9f9f9f",
                    highlight: "#9f9f9f",
                    label: fb
                },
                {
                    value: userParticipationKikavgprect,
                    color: "#0066cb",
                    highlight: "#0066cb",
                    label: kik
                }
            ];

            var doughnutOptions = {
                segmentShowStroke: true,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 2,
                percentageInnerCutout: 0, // This is 0 for Pie charts
                animationSteps: 100,
                animationEasing: "easeOutBounce",
                animateRotate: true,
                animateScale: false,
                responsive: true
            };

            var ctx = document.getElementById("doughnutChart").getContext("2d");
            var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);





            // Options for Views and Performance Chart
            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Line 2",
                        fillColor: "rgba(159,159,159,0.5)",
                        strokeColor: "rgba(159,159,159,1)",
                        pointColor: "rgba(159,159,159,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(159,159,159,1)",
                        data: [22, 44, 67, 43, 76, 45, 12]
                    },
                    {
                        label: "Line 1",
                        fillColor: "rgba(0,102,203,0.5)",
                        strokeColor: "rgba(0,102,203,0.7)",
                        pointColor: "rgba(0,102,203,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: [16, 32, 18, 26, 42, 33, 44]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 20,
                datasetStroke : true,
                datasetStrokeWidth : 1,
                datasetFill : true,
                responsive: true
            };

            var ctx = document.getElementById("lineOptions-by-chat-platform").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);








            // Options for country












            // Options for gender and Age
            var barOptions = {
                scaleBeginAtZero : true,
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.05)",
                scaleGridLineWidth : 1,
                barShowStroke : true,
                barStrokeWidth : 1,
                barValueSpacing : 10,
                barDatasetSpacing : 1,
                responsive:true
            };

            var barData = {
                labels: ["12 or less", "12 to 18", "19 to 25", "26 to 35", "36 to 45", "46 or more"],
                datasets: [
                    {
                        label: "Male",
                        fillColor: "rgba(76,148,218,1)",
                        strokeColor: "rgba(76,148,218,1)",
                        highlightFill: "rgba(76,148,218,0.75)",
                        highlightStroke: "rgba(76,148,218,1)",
                        data: [65, 59, 80, 81, 56, 55]
                    },
                    {
                        label: "Female",
                        fillColor: "rgba(233,127,117,1)",
                        strokeColor: "rgba(233,127,117,1)",
                        highlightFill: "rgba(233,127,117,0.75)",
                        highlightStroke: "rgba(233,127,117,1)",
                        data: [28, 48, 40, 19, 86, 27]
                    }
                ]
            };

            var ctx = document.getElementById("barOptions-gender-age").getContext("2d");
            var myNewChart = new Chart(ctx).Bar(barData, barOptions); 








            // Views and Performance
            /*var data1 = [ [0, 55], [1, 48], [2, 40], [3, 36], [4, 40], [5, 60], [6, 50], [7, 51] ];
            var data2 = [ [0, 56], [1, 49], [2, 41], [3, 38], [4, 46], [5, 67], [6, 57], [7, 59] ];

            var chartUsersOptions = {
                series: {
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    }
                },
                grid: {
                    tickColor: "#f0f0f0",
                    borderWidth: 1,
                    borderColor: 'f0f0f0',
                    color: '#6f6f6f'
                },
                colors: [ "#0066cb", "#9f9f9f"]
            };

            $.plot($("#chart-views-performance"), [data1, data2], chartUsersOptions);*/





            // Flot charts 2 data and options
            /*var chartIncomeData = [
                {
                    label: "line",
                    data: [ [1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
                }
            ];

            var chartIncomeOptions = {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 0,
                        fill: true,
                        fillColor: "#64cc34"

                    }
                },
                colors: ["#62cb31"],
                grid: {
                    show: false
                },
                legend: {
                    show: false
                }
            };

            $.plot($("#flot-income-chart"), chartIncomeData, chartIncomeOptions);


            // Pie charts data and options used in many views

            $("span.pie").peity("pie", {
                fill: ["#62cb31", "#edf0f5"]
            });

            // Initialize iCheck plugin
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });*/


            // Initialize tooltips
            $('body').tooltip({
                selector: "[data-toggle=tooltip]"
            });

            // Initialize popover
            $("[data-toggle=popover]").popover({
                trigger: 'focus'
            })

        }

    });

});

Template.dashboard.events({

    'click .showhide': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        var icon = $(event.target).closest('i');
        var body = hpanel.find('div.panel-body');
        var footer = hpanel.find('div.panel-footer');
        body.slideToggle(300);
        footer.slideToggle(200);

        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        hpanel.toggleClass('').toggleClass('panel-collapse');
        setTimeout(function () {
            hpanel.resize();
            hpanel.find('[id^=map-]').resize();
        }, 50);
    },

    'click .closebox': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        hpanel.remove();




    },

    'click .run-tour': function(){
        // Instance the tour
        var tourAnalytics = new Tour({
            backdrop: true,
            onShown: function(tourAnalytics) {

                // ISSUE    - https://github.com/sorich87/bootstrap-tour/issues/189
                // FIX      - https://github.com/sorich87/bootstrap-tour/issues/189#issuecomment-49007822

                // You have to write your used animated effect class
                // Standard animated class
                $('.animated').removeClass('fadeIn');
                // Animate class from animate-panel plugin
                $('.animated-panel').removeClass('fadeIn');

            },
            steps: [
                {
                    element: ".tour-1",
                    title: "User Engagement",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-2",
                    title: "Campaign Views",
                    content: "Number of users that viewed your campaign pre engagement.",
                    placement: "top"

                },
                {
                    element: ".tour-3",
                    title: "Audience Growth",
                    content: "Monitoring your success over time.",
                    placement: "top"

                },
                {
                    element: ".tour-4",
                    title: "Most Active Chat Platform",
                    content: "Monitoring views by platform.",
                    placement: "top"

                },
                {
                    element: ".tour-5",
                    title: "Views and Performance",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-6",
                    title: "Response Rate",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-7",
                    title: "Views and Performance by Chat Platform",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                },
                {
                    element: ".tour-8",
                    title: "Activity by Chat Platform",
                    content: "Growth and success over time in each one of the platform included in your campaign.",
                    placement: "top"

                },
                {
                    element: ".tour-9",
                    title: "Activity by Location",
                    content: "A global view of your success.",
                    placement: "top"

                },
                {
                    element: ".tour-10",
                    title: "Audience Segmentation by Gender and Age",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "top"

                }
            ]});

        // Initialize the tour
        tourAnalytics.init();
        tourAnalytics.restart();
    }


    /*,
    var dataset = Tracks.find( {"campaignName": "w3" }  );

    console.log("dat="+dataset);

    var json = JSON.stringify(dataset);
    console.log ("Fetch platform="+  json );

    var msgs = JSON.parse(json);


    console.log ("Parse campaign="+  msgs.obj );
    */

});









