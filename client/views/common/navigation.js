var pageSession = new ReactiveDict();

;

var navigationViewItems = function(cursor) {
    if(!cursor) {
        return [];
    }

    var searchString = pageSession.get("navigationViewSearchString");
    var sortBy = pageSession.get("navigationViewSortBy");
    var sortAscending = pageSession.get("navigationViewSortAscending");
    if(typeof(sortAscending) == "undefined") sortAscending = true;

    var raw = cursor.fetch();

    // filter
    var filtered = [];
    if(!searchString || searchString == "") {
        filtered = raw;
    } else {
        searchString = searchString.replace(".", "\\.");
        var regEx = new RegExp(searchString, "i");
        var searchFields = ["name", "description", "start date", "phone", "email", "note", "active"];
        filtered = _.filter(raw, function(item) {
            var match = false;
            _.each(searchFields, function(field) {
                var value = (getPropertyValue(field, item) || "") + "";

                match = match || (value && value.match(regEx));
                if(match) {
                    return false;
                }
            })
            return match;
        });
    }

    // sort
    if(sortBy) {
        filtered = _.sortBy(filtered, sortBy);

        // descending?
        if(!sortAscending) {
            filtered = filtered.reverse();
        }
    }

    return filtered;
};

var navigationViewExport = function(cursor, fileType) {
    var data = navigationViewItems(cursor);
    var exportFields = ["name", "description", "start date", "phone", "email", "note", "active"];

    var str = convertArrayOfObjects(data, exportFields, fileType);

    var filename = "export." + fileType;

    downloadLocalResource(str, filename, "application/octet-stream");
}


Template.navigationView.rendered = function() {
    pageSession.set("navigationViewStyle", "table");

};

Template.navigationView.events({
    "submit #dataview-controls": function(e, t) {
        return false;
    },

    "click #dataview-search-button": function(e, t) {
        e.preventDefault();
        var form = $(e.currentTarget).parent();
        if(form) {
            var searchInput = form.find("#dataview-search-input");
            if(searchInput) {
                searchInput.focus();
                var searchString = searchInput.val();
                pageSession.set("navigationViewSearchString", searchString);
            }

        }
        return false;
    },

    "keydown #dataview-search-input": function(e, t) {
        if(e.which === 13)
        {
            e.preventDefault();
            var form = $(e.currentTarget).parent();
            if(form) {
                var searchInput = form.find("#dataview-search-input");
                if(searchInput) {
                    var searchString = searchInput.val();
                    pageSession.set("navigationViewSearchString", searchString);
                }

            }
            return false;
        }

        if(e.which === 27)
        {
            e.preventDefault();
            var form = $(e.currentTarget).parent();
            if(form) {
                var searchInput = form.find("#dataview-search-input");
                if(searchInput) {
                    searchInput.val("");
                    pageSession.set("navigationViewSearchString", "");
                }

            }
            return false;
        }

        return true;
    },

    "click #dataview-insert-button": function(e, t) {
        e.preventDefault();
        Router.go("campaigns.insert", {});
    },

    "click #dataview-export-default": function(e, t) {
        e.preventDefault();
        navigationViewExport(this.campaigns, "csv");
    },

    "click #dataview-export-csv": function(e, t) {
        e.preventDefault();
        navigationViewExport(this.campaigns, "csv");
    },

    "click #dataview-export-tsv": function(e, t) {
        e.preventDefault();
        navigationViewExport(this.campaigns, "tsv");
    },

    "click #dataview-export-json": function(e, t) {
        e.preventDefault();
        navigationViewExport(this.campaigns, "json");
    }


});

Template.navigationView.helpers({



    "isEmpty": function() {
        return !this.campaigns || this.campaigns.count() == 0;
    },
    "isNotEmpty": function() {
        return this.campaigns && this.campaigns.count() > 0;
    },
    "isNotFound": function() {
        return this.campaigns && pageSession.get("navigationViewSearchString") && navigationViewItems(this.campaigns).length == 0;
    },
    "searchString": function() {
        return pageSession.get("navigationViewSearchString");
    },
    "viewAsTable": function() {
        return pageSession.get("navigationViewStyle") == "table";
    },
    "viewAsList": function() {
        return pageSession.get("navigationViewStyle") == "list";
    },
    "viewAsGallery": function() {
        return pageSession.get("navigationViewStyle") == "gallery";
    }


});


Template.navigationViewTable.rendered = function() {

};

Template.navigationViewTable.events({
    "click .th-sortable": function(e, t) {
        e.preventDefault();
        var oldSortBy = pageSession.get("navigationViewSortBy");
        var newSortBy = $(e.target).attr("data-sort");

        pageSession.set("navigationViewSortBy", newSortBy);
        if(oldSortBy == newSortBy) {
            var sortAscending = pageSession.get("navigationViewSortAscending") || false;
            pageSession.set("navigationViewSortAscending", !sortAscending);
        } else {
            pageSession.set("navigationViewSortAscending", true);
        }
    }
});

Template.navigationViewTable.helpers({
    "tableItems": function() {
        return navigationViewItems(this.campaigns);
    }
});


Template.navigationViewTableItems.rendered = function() {

};

Template.navigationViewTableItems.events({
    "click td": function(e, t) {
        e.preventDefault();

        Router.go("campaigns.details", {campaignId: this._id});
        return false;
    },

    "click .inline-checkbox": function(e, t) {
        e.preventDefault();

        if(!this || !this._id) return false;

        var fieldName = $(e.currentTarget).attr("data-field");
        if(!fieldName) return false;

        var values = {};
        values[fieldName] = !this[fieldName];

        Campaigns.update({ _id: this._id }, { $set: values });

        return false;
    },

    "click #delete-button": function(e, t) {
        e.preventDefault();
        var me = this;
        bootbox.dialog({
            message: "Delete? Are you sure?",
            title: "Delete",
            animate: false,
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn-success",
                    callback: function() {
                        Campaigns.remove({ _id: me._id });
                    }
                },
                danger: {
                    label: "No",
                    className: "btn-default"
                }
            }
        });
        return false;
    },
    "click #edit-button": function(e, t) {
        e.preventDefault();
        Router.go("campaigns.edit", {campaignId: this._id});
        return false;
    }
});

Template.navigationViewTableItems.helpers({
    "checked": function (value) {
        return value ? "checked" : ""
    }
});

Template.navigation.onRendered(function() {
    // Initialize metsiMenu plugin to sidebar menu
    $('#side-menu').metisMenu();

    // Sparkline bar chart data and options used under Profile image on navigation
    $("#sparkline1").sparkline([5, 6, 7, 2, 0, 4, 2, 4, 5, 7, 2, 4, 12, 11, 4], {
        type: 'bar',
        barWidth: 7,
        height: '30px',
        barColor: '#62cb31',
        negBarColor: '#53ac2a'
    });

});

Template.navigation.events({

    // Colapse menu in mobile mode after click on element
    'click #side-menu a:not([href$="\\#"])': function(){
        if ($(window).width() < 769) {
            $("body").toggleClass("show-sidebar");
        }
    }

});



