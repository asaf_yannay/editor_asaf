var pageSession = new ReactiveDict();

var fb = "Facebook";
var kik = "Kik";

Template.mostActiveChatPlatform.userParticipation = function() {
    return Campaigns.find().count();
};

Template.mostActiveChatPlatform.campViewAvg = function() {
    return Tracks.find().count();
};

Template.mostActiveChatPlatform.userParticipationPrecent = function() {
    return Tracks.find().count() * 100;
};

Template.mostActiveChatPlatform.userParticipationFBavgprect = function() {
    userParticipationFBavgprect = Tracks.find({platform:  fb}).count() / Tracks.find().count() * 100;
    return userParticipationFBavgprect
};

Template.mostActiveChatPlatform.userParticipationKikavgprect = function() {
    userParticipationKikavgprect =  Tracks.find({platform:  kik}).count() / Tracks.find().count() * 100;
    return userParticipationKikavgprect;
};


Template.mostActiveChatPlatform.userParticipationPrecent_FB = function() {
    return Tracks.find({platform:  fb}).count() * 100;
};

Template.mostActiveChatPlatform.userParticipationPrecent_Kik = function() {
    return Tracks.find({platform:  kik}).count() * 100;
};

function getMostActiveChatPlatform(){
    var facebookCount = Tracks.find({platform:  fb} ).count();
    var kikCount      = Tracks.find({platform:  kik}      ).count();

    if( facebookCount > kikCount ){
        return "Facebook";
    }else{
        return "Kik";
    }
}

Template.mostActiveChatPlatform.mostActiveChatPlatform = function() {
    return getMostActiveChatPlatform();
};


Template.mostActiveChatPlatform.mostActiveByIcon = function() {
    if ( getMostActiveChatPlatform() == "Facebook" ){
        return "/images/apps/app-logo-messenger.png";
    }else
    if ( getMostActiveChatPlatform() == "Kik" ){
        return "/images/apps/app-logo-kik.png";
    }
};

Template.mostActiveChatPlatform.mostActiveByView = function() {
    if ( getMostActiveChatPlatform() == "Facebook" ){
        return Tracks.find({platform:  "Facebook"} ).count();
    }else
    if ( getMostActiveChatPlatform() == "Kik" ){
        return Tracks.find({platform:  "Kik"}      ).count();
    }
};

Template.mostActiveChatPlatform.onRendered(function() {


    var handle = Meteor.subscribe("tracks_single");


    Tracker.autorun(function () {
        if (handle.ready()) {
            var dat = Tracks.find({platform: "Facebook"}, {fields: {platform: 1, campaignName: 1}}).fetch();
            var facebookCount = Tracks.find({platform: "Facebook"}, {fields: {platform: 1, campaignName: 1}}).count();
            //var dat = Tracks.find({} ,{fields: {platform:1, campaignName:1}}).fetch();
            console.log(dat);
            console.log(dat[0].campaignName);
            console.log("FB count=" + facebookCount);

            var labels = dat.map(function (d) {
                return d.platform
            });
            var data = dat.map(function (d) {
                return d.campaignName
            });
            console.log("labels=" + labels + " data==" + data);

            var userParticipationFBavgprect = Tracks.find({platform: fb}).count() / Tracks.find().count() * 100;
            var userParticipationKikavgprect = Tracks.find({platform: kik}).count() / Tracks.find().count() * 100;

            console.log("FB = " + userParticipationFBavgprect);
            console.log("Kik = " + userParticipationKikavgprect);

            //userParticipation = Tracks.find().count();
        }
    });
});