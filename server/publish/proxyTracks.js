Meteor.publish("proxytracks", function() {
    return ProxyTracks.find({}, {});
});

Meteor.publish("proxytracks_empty", function() {
    return ProxyTracks.find({_id:null}, {});
});

Meteor.publish("proxytracks", function(proxyTrackId) {
    return ProxyTracks.find({_id:proxyTrackId}, {});
});