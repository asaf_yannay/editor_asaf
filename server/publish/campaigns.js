Meteor.publish("campaigns", function() {
		return Campaigns.find({}, {});
});

Meteor.publish("campaigns_empty", function() {
	return Campaigns.find({_id:null}, {});
});

Meteor.publish("campaign", function(campaignId) {
	return Campaigns.find({_id:campaignId}, {});
});

