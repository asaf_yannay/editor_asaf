ProxyTracks.allow({
    insert: function (userId, doc) {
        return ProxyTracks.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return ProxyTracks.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return ProxyTracks.userCanRemove(userId, doc);
    }
});

ProxyTracks.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.createdBy) doc.createdBy = userId;
});

ProxyTracks.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

ProxyTracks.before.remove(function(userId, doc) {

});

ProxyTracks.after.insert(function(userId, doc) {

});

ProxyTracks.after.update(function(userId, doc, fieldNames, modifier, options) {

});

ProxyTracks.after.remove(function(userId, doc) {

});